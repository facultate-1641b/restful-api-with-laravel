<?php


namespace App\Exceptions;

use Illuminate\Database\Eloquent\ModelNotFoundException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

trait ExceptionTrait
{
    public function apiException($request, $e)
    {
        if ($e instanceof ModelNotFoundException)
        {
            return response()->json([
                'error' => 'Product Model not found'
            ], \Symfony\Component\HttpFoundation\Response::HTTP_NOT_FOUND);
        }
        if ($e instanceof NotFoundHttpException) {
            return response()->json([
                'error' => 'Incorect route'
            ], \Symfony\Component\HttpFoundation\Response::HTTP_NOT_FOUND);
        }
        return parent::render($request, $e);
    }
}
